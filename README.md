## Compilation
- 'gradle clean fatJar'

## Preparation
- Copy src/main/resources/carconnectionscripts/sendcmd2robotcar.sh /usr/bin/sendcmd2robotcar.sh (or somewhere else in the PATH!)

### Without RobotCar-Hardware
- Copy src/main/resources/carconnectionscripts/reconnect2robotcar-simulation.sh /usr/bin/reconnect2robotcar.sh (or somewhere else in the PATH!)

### With RobotCar-Hardware
- Copy src/main/resources/carconnectionscripts/reconnect2robotcar.sh /usr/bin/reconnect2robotcar.sh (or somewhere else in the PATH!)

## Execution
- cd 'build/lib/'
- 'java -jar robot-car-core.jar'



