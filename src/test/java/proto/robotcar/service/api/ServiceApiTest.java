package proto.robotcar.service.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.concurrent.Executors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.robotcar.service.RobotCarServer;
import proto.robotcar.service.RobotCarServiceInjector;
import proto.robotcar.service.api.CarControllerServiceGrpc.CarControllerServiceBlockingStub;
import proto.robotcar.service.api.TrafficLightControllerServiceGrpc.TrafficLightControllerServiceBlockingStub;
import proto.robotcar.service.api.TrafficLightControllerServiceGrpc.TrafficLightControllerServiceStub;

public class ServiceApiTest {

	private static ManagedChannel channel;
	private static RobotCarServer server;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		Executors.newSingleThreadExecutor().execute(new Runnable() {
		    @Override 
		    public void run() {
				Injector injector = Guice.createInjector(new RobotCarServiceInjector());		
				
				server = injector.getInstance(RobotCarServer.class);
				server.run();
		    }
		});
		
		Thread.sleep(5000);
		
		channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50051).usePlaintext(true).build();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		channel.shutdown();
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStart() {
		CarControllerServiceBlockingStub stub = CarControllerServiceGrpc.newBlockingStub(channel);
		assertNotNull(stub);
		
		EmptyRequest request = EmptyRequest.newBuilder().build();
		
		SucceededReply response = stub.start(request);
		
		assertTrue(response.getSuccess());
	}

	@Test
	public void testStop() {
		CarControllerServiceBlockingStub stub = CarControllerServiceGrpc.newBlockingStub(channel);
		assertNotNull(stub);
		
		EmptyRequest request = EmptyRequest.newBuilder().build();
		
		SucceededReply response = stub.stop(request);
		
		assertTrue(response.getSuccess());
	}

	@Test
	public void testTrafficLightSync() {
		
		proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightState[] validValues = proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightState.values();
		final int NUMBER_OF_CHECKS = 5;
		int previous = -1;
		
		TrafficLightControllerServiceBlockingStub stub = TrafficLightControllerServiceGrpc.newBlockingStub(channel);
		ClientId request = ClientId.newBuilder().setName("TestClient").build();
		
		Iterator<TrafficLightState> values = stub.subscribeOnTrafficLightState(request);
		
		for(int i=0; i<NUMBER_OF_CHECKS && values.hasNext(); i++) {
			TrafficLightState item = values.next();
			assertTrue(item.getStateValue() >= validValues[0].ordinal() && item.getStateValue() <= validValues.length);
			
//			System.out.println("i: "+i+", p: "+previous+", a: "+item.getStateValue()+", v:"+validValues.length);
			
			if(i > 0) {
				if(previous == validValues.length-1) {
					assertEquals(0, item.getStateValue());
				}
				else {
					assertEquals(previous + 1, item.getStateValue());
				}
			}
			
			previous = item.getStateValue();
		}
		
	}

//	@Test
//	public void testTrafficLight() {
//		TrafficLightControllerServiceStub stub = TrafficLightControllerServiceGrpc.newStub(channel);
//        ClientId request = ClientId.newBuilder().setName("TestClient").build();
//
//		stub.subscribeOnTrafficLightState(request, new StreamObserver<TrafficLightState>() {
//						
//			@Override
//			public void onNext(TrafficLightState value) {
//				// TODO Auto-generated method stub		
//				System.out.println("Count "+count++);
//			}
//			
//			@Override
//			public void onError(Throwable t) {
//				// TODO Auto-generated method stub				
//			}
//			
//			@Override
//			public void onCompleted() {
//				// TODO Auto-generated method stub				
//			}
//		});
//	}

	
}
