package proto.robotcar.service.caraccess.carconnector.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proto.robotcar.service.caraccess.carconnector.impl.CarMessageParserImpl;

public class CarMessageParserListenerTest implements CarMessageListener {

	private String message;
	private CarValueData carData;
	private CarMessageParserImpl parserImpl;
	private long currentMillis;

	@Before
	public void setup() {
		parserImpl = new CarMessageParserImpl();
	}

	@Test
	public void withListener() throws Exception {
		parserImpl.addCarMessageListener(this);
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'log','data':{'id':'CarMessageParserParsingTest','level':'debug','msg':'log-message'}}"));

		assertEquals("CarLogData(id=CarMessageParserParsingTest, level=debug, msg=log-message)", message);
		assertNull(carData);
	}

	private String escapeSingleQuotes(final String value) {
		return value.replaceAll("'", "\"");
	}

	@Test
	public void withoutListener() throws Exception {
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'log','data':{'id':'CarMessageParserParsingTest','level':'debug','msg':'log-message'}}"));

		assertNull(message);
		assertNull(carData);
		assertEquals(0, currentMillis);
	}

	@Test
	public void withRemovedListener() throws Exception {
		parserImpl.addCarMessageListener(this);
		parserImpl.removeCarMessageListener(this);
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'log','data':{'id':'CarMessageParserParsingTest','level':'debug','msg':'log-message'}}"));

		assertNull(message);
		assertNull(carData);
		assertEquals(0, currentMillis);
	}

	public void cleanup() {
	}

	@Override
	public void onNewLogMessage(String message) {
		this.message = message;
	}

	@Override
	public void onNewCarValueData(CarValueData carData) {
		this.carData = carData;
	}

	@Override
	public void onNewHeartbeat(long currentMillis) {
		this.currentMillis = currentMillis;
	}

}
