package proto.robotcar.service.caraccess.carconnector.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proto.robotcar.service.caraccess.carconnector.impl.CarMessageParserImpl;

public class CarMessageParserParsingTest implements CarMessageListener {

	private String message;
	private CarValueData carData;
	private CarMessageParserImpl parserImpl;
	private long currentMillis;

	@Before
	public void setup() {
		parserImpl = new CarMessageParserImpl();
		parserImpl.addCarMessageListener(this);
	}
	
	@Test
	public void emptyString() throws Exception {
		parserImpl.parseMessage("");
		
		assertNull(message);
		assertNull(carData);
		assertEquals(0, currentMillis);
	}

	@Test
	public void noValidJson() throws Exception {
		parserImpl.parseMessage(escapeSingleQuotes("{'type':''"));
		
		assertNull(message);
		assertNull(carData);
		assertEquals(0, currentMillis);
	}
	
	private String escapeSingleQuotes(final String value)
	{
		return value.replaceAll("'", "\"");
	}
	
	@Test
	public void logMessage() throws Exception {
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'log','data':{'id':'CarMessageParserParsingTest','level':'debug','msg':'log-message'}}"));
		
		assertEquals("CarLogData(id=CarMessageParserParsingTest, level=debug, msg=log-message)", message);
		assertNull(carData);
		assertEquals(0, currentMillis);
	}
	
	@Test
	public void carValueMessage() throws Exception {
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'CARDATA','data':{'drivingState':'STOP','drivingDirection':'FORWARD'}}"));
		
		assertEquals(new CarValueData("STOP", "FORWARD"), carData);
		assertNull(message);
		assertEquals(0, currentMillis);
	}
	
	@Test
	public void heartbeat() throws Exception {
		parserImpl.parseMessage(escapeSingleQuotes("{'type':'HEARTBEAT','data':'1234'}"));
		
		assertEquals(1234L, currentMillis);
		assertNull(carData);
		assertNull(message);
	}
	
	public void cleanup()
	{
		parserImpl.removeCarMessageListener(this);
	}
	
	@Override
	public void onNewLogMessage(String message) {
		this.message = message;
	}

	@Override
	public void onNewCarValueData(CarValueData carData) {
		this.carData = carData;
	}

	@Override
	public void onNewHeartbeat(long currentMillis) {
		this.currentMillis = currentMillis;
	}
	
}
