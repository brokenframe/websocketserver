package proto.robotcar.service;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class ServiceMain {
	
	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new RobotCarServiceInjector());		
	
		RobotCarServer server = injector.getInstance(RobotCarServer.class);
		server.run();
	}

}
