package proto.robotcar.service;

import java.io.IOException;
import java.util.Scanner;

import com.google.inject.Inject;

import io.grpc.Server;
import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.caraccess.carconnector.api.CarConnectorService;
import proto.robotcar.service.core.carcontroller.api.CarController;
import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightController;

@Slf4j
public class RobotCarServer {

	@Inject
	private TrafficLightController trafficLightController;

	@Inject
	private CarConnectorService carConnectorService;

	@Inject
	private RobotCarServiceEndpoint endpoint;
	
	@SuppressWarnings("unused")
	@Inject
	private CarController carController;	
	
	@SuppressWarnings("unused")
	@Inject
	private CarDataProvider carDataProvider;

	public void run() {
		log.info("Run RobotCarServer.");
		try {
			startServices();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitForQuitCommand();
		stopServices();
		log.info("Stop RobotCarServer.");
	}

	private void startServices() throws IOException, InterruptedException {
		trafficLightController.start();
		carConnectorService.connect();
		endpoint.start();
	}
	
	private void waitForQuitCommand() {
		System.out.println("Enter 'q' or 'quit' to exit.");
		Scanner scanner = new Scanner(System.in);
		while (true) {
			final String nextCommand = scanner.next();
			log.debug("Command was " + nextCommand);
			if (nextCommand.equals("q") || nextCommand.equals("quit"))
				break;
		}
		scanner.close();
	}
	
	private void stopServices() {
		endpoint.stop();
		trafficLightController.stop();
		carConnectorService.disconnect();
	}
}
