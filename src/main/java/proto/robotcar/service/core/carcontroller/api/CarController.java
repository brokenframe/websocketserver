package proto.robotcar.service.core.carcontroller.api;

public interface CarController {

	void start();
	
	void stop();
}
