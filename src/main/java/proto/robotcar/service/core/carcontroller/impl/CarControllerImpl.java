package proto.robotcar.service.core.carcontroller.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import proto.robotcar.service.caraccess.carconnector.api.CarConnectorService;
import proto.robotcar.service.core.carcontroller.api.CarController;

@Singleton
public class CarControllerImpl implements CarController {

	@Inject
	private CarConnectorService carConnector;
	
	@Override
	public void start() {
		carConnector.startDriving();
	}

	@Override
	public void stop() {
		carConnector.stopDriving();
	}

}
