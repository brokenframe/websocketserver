package proto.robotcar.service.core.errorsimulator.api;

public interface ErrorListener {

	void onErrorActivated(ErrorCase errorCase);
	
	void onErrorDeactivated(ErrorCase errorCase);
	
}
