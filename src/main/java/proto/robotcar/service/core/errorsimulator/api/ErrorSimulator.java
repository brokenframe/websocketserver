package proto.robotcar.service.core.errorsimulator.api;

public interface ErrorSimulator {

	void addErrorListener(ErrorListener errorListener);

	void activateError(ErrorCase errorCase);
	
	void deactivateError(ErrorCase errorCase);
	
	void removeErrorListener(ErrorListener errorListener);
}
