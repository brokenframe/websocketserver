package proto.robotcar.service.core.cardataprovider.impl;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import proto.robotcar.service.caraccess.carconnector.api.CarConnectorService;
import proto.robotcar.service.caraccess.carconnector.api.CarMessageListener;
import proto.robotcar.service.caraccess.carconnector.api.CarValueData;
import proto.robotcar.service.core.cardataprovider.api.CarDataListener;
import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;

@Singleton
public class CarDataProviderImpl implements CarDataProvider, CarMessageListener {

	private Set<CarDataListener> listeners = new CopyOnWriteArraySet<>();

	@Inject
	private CarConnectorService carConnectionService;

	@Override
	public void addCarDataListener(CarDataListener listener) {
		if(listeners.isEmpty())
			carConnectionService.addCarMessageListener(this);
			
		listeners.add(listener);
	}

	@Override
	public void removeCarDataListener(CarDataListener listener) {
		listeners.remove(listener);
		
		if(listeners.isEmpty())
			carConnectionService.removeCarMessageListener(this);

	}

	@Override
	public void onNewLogMessage(String message) {
	}

	@Override
	public void onNewCarValueData(CarValueData carData) {
		listeners.stream().forEach(l -> l.onNewCarDataReceived(carData));
	}

	@Override
	public void onNewHeartbeat(long currentMillis) {
		listeners.stream().forEach(l -> l.onBTConnectionToCarUp());
	}

}
