package proto.robotcar.service.core.cardataprovider.api;

public interface CarDataProvider {

	void addCarDataListener(CarDataListener listener);
	
	void removeCarDataListener(CarDataListener listener);
	
}
