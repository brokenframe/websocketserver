package proto.robotcar.service.core.cardataprovider.api;

import proto.robotcar.service.caraccess.carconnector.api.CarValueData;

public interface CarDataListener {

	void onNewCarDataReceived(CarValueData carData);
	
	void onBTConnectionToCarUp();
	
}
