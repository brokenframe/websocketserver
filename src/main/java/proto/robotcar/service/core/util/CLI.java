package proto.robotcar.service.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CLI {

	private static Logger logger = LoggerFactory.getLogger(CLI.class);

	public static String syncExecute(String command) {

		StringBuffer output = new StringBuffer();

		Process proc;
		try {
			proc = Runtime.getRuntime().exec(command);
			proc.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return output.toString();
	}
}
