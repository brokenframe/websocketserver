package proto.robotcar.service.core.trafficlightcontroller.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.google.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightController;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightListener;

@Slf4j
@Singleton
public class TrafficLightControllerServiceImpl implements TrafficLightController {

	private final TrafficLightControllerImpl trafficLightControllerImpl = new TrafficLightControllerImpl();
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	@Override
	public void addTrafficLightListener(TrafficLightListener trafficLightListener) {
		trafficLightControllerImpl.addTrafficLightListener(trafficLightListener);
	}

	public void start() {
		executor.submit(trafficLightControllerImpl);
	}

	public void stop() {
		log.info("Start stopping.");
		executor.shutdownNow();
		log.info("End stopping.");
	}

	@Override
	public void removeTrafficLightListener(TrafficLightListener trafficLightListener) {
		trafficLightControllerImpl.removeTrafficLightListener(trafficLightListener);
	}

}
