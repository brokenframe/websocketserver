package proto.robotcar.service.core.trafficlightcontroller.api;

public interface TrafficLightListener {

	void onStateChanged(TrafficLightState trafficLightState);
	
}
