package proto.robotcar.service.core.trafficlightcontroller.api;

public interface TrafficLightController {

	void addTrafficLightListener(TrafficLightListener trafficLightListener);
	
	void start();
	
	void stop();
	
	void removeTrafficLightListener(TrafficLightListener trafficLightListener);
	
}
