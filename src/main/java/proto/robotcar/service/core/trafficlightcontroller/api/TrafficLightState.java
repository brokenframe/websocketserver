package proto.robotcar.service.core.trafficlightcontroller.api;

public enum TrafficLightState {

	RED, REDYELLOW, GREEN, YELLOW
}
