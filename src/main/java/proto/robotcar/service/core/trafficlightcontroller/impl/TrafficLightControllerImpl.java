package proto.robotcar.service.core.trafficlightcontroller.impl;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightListener;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightState;
import proto.robotcar.service.core.util.CLI;

@Slf4j
public class TrafficLightControllerImpl implements Runnable {

	private final Set<TrafficLightListener> listeners = new CopyOnWriteArraySet<>();

	@Override
	public void run() {
		Thread.currentThread().setName("TrafficLightControllerService");
		while (true) {
			try {
				redPhase(3);
				redYellowPhase(1);
				greenPhase(3);
				yellowPhase(1);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	private void redPhase(int seconds) throws InterruptedException {
		callTrafficLightApi("1 0 0");
		callListeners(TrafficLightState.RED);
		trySleep(seconds);
	}

	private void trySleep(int seconds) throws InterruptedException {
		Thread.sleep(seconds * 1000);
	}

	private void redYellowPhase(int seconds) throws InterruptedException {
		callTrafficLightApi("1 1 0");
		callListeners(TrafficLightState.REDYELLOW);
		trySleep(seconds);
	}

	private void greenPhase(int seconds) throws InterruptedException {
		callTrafficLightApi("0 0 1");
		callListeners(TrafficLightState.GREEN);
		trySleep(seconds);
	}

	private void yellowPhase(int seconds) throws InterruptedException {
		callTrafficLightApi("0 1 0");
		callListeners(TrafficLightState.YELLOW);
		trySleep(seconds);
	}

	private void callListeners(TrafficLightState state) {
		listeners.stream().forEach(listener -> listener.onStateChanged(state));
	}

	private void callTrafficLightApi(String params) {
		CLI.syncExecute("python raspi3-python-traffic-light/traffic-light.py " + params);
	}

	public void addTrafficLightListener(TrafficLightListener trafficLightListener) {
		listeners.add(trafficLightListener);
		log.info("addTrafficLightListener");
	}

	public void removeTrafficLightListener(TrafficLightListener trafficLightListener) {
		listeners.remove(trafficLightListener);
		log.info("removeTrafficLightListener");
	}
}
