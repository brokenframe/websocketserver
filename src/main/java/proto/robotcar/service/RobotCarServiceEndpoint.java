package proto.robotcar.service;

import java.io.IOException;

import com.google.inject.Inject;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import proto.robotcar.service.api.CarControllerServiceGrpc.CarControllerServiceImplBase;
import proto.robotcar.service.stubs.CarDataProviderServiceImpl;
import proto.robotcar.service.stubs.TrafficLightCtrlServiceImpl;

public class RobotCarServiceEndpoint {

	private Server server;
	
	@Inject
	CarControllerServiceImplBase carControllerService;
	
	public void start() throws IOException, InterruptedException {
		/* The port on which the server should run */

		int port = 50051;
		server = ServerBuilder.forPort(port).addService(carControllerService)
				.addService(new TrafficLightCtrlServiceImpl())
				.addService(new CarDataProviderServiceImpl())
				.build().start();
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Use stderr here since the logger may have been reset by its
				// JVM shutdown hook.
				System.err.println("*** shutting down gRPC server since JVM is shutting down");
				stop();
				System.err.println("*** server shut down");
			}
		});
		
		blockUntilShutdown();
	}

	public void stop() {
		if (server != null) {
			server.shutdown();
		}
	}

	/**
	 * Await termination on the main thread since the grpc library uses daemon
	 * threads.
	 */
	private void blockUntilShutdown() throws InterruptedException {
		if (server != null) {
			server.awaitTermination();
		}
	}
	
	
}
