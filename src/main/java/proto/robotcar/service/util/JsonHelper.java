package proto.robotcar.service.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public final class JsonHelper {
	  private static final Gson gson = new Gson();

	  public static boolean isValidJson(String jsonInString) {
	      try {
	          gson.fromJson(jsonInString, Object.class);
	          return true;
	      } catch(JsonSyntaxException e) { 
	          return false;
	      }
	  }
}

