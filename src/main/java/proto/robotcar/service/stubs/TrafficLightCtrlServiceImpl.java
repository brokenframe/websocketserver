package proto.robotcar.service.stubs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.api.ClientId;
import proto.robotcar.service.api.EmptyMessage;
import proto.robotcar.service.api.TrafficLightControllerServiceGrpc.TrafficLightControllerServiceImplBase;
import proto.robotcar.service.api.TrafficLightState;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightListener;
import proto.robotcar.service.core.trafficlightcontroller.impl.TrafficLightControllerImpl;

@Slf4j
public class TrafficLightCtrlServiceImpl extends TrafficLightControllerServiceImplBase implements TrafficLightListener {


	private final TrafficLightControllerImpl trafficLightControllerImpl;
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
				
	Map<ClientId, StreamObserver<TrafficLightState>> listener = new HashMap<ClientId, StreamObserver<TrafficLightState>>();
	
	public TrafficLightCtrlServiceImpl() {
		trafficLightControllerImpl = new TrafficLightControllerImpl();
		trafficLightControllerImpl.addTrafficLightListener(this);
		start();
	}


	@Override
	public void subscribeOnTrafficLightState(ClientId request,
			StreamObserver<TrafficLightState> responseObserver) {
		listener.put(request, responseObserver);
	}


	@Override
	public void unsubscribeOnTrafficLightState(ClientId request, StreamObserver<EmptyMessage> responseObserver) {
		
		// stop the subscription and inform the client about it
		listener.get(request).onCompleted();
		
		// remove the client from the listener list
		listener.remove(request);
		
		// answer to the unsubscribe request to the client
		responseObserver.onNext(EmptyMessage.newBuilder().build());
		responseObserver.onCompleted();
	}


	@Override
	public void onStateChanged(
			
		proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightState trafficLightState) {

		log.info(trafficLightState.toString());
		
		TrafficLightState value = TrafficLightState.newBuilder().setStateValue(trafficLightState.ordinal()).build();
		
		Iterator<Entry<ClientId, StreamObserver<TrafficLightState>>> it = listener.entrySet().iterator();
		while(it.hasNext()) {
			it.next().getValue().onNext(value);
		}
				
	}


	public void start() {
		// TODO Auto-generated method stub
		executor.submit(trafficLightControllerImpl);
	}


	
	
}
