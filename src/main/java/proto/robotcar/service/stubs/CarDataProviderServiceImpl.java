package proto.robotcar.service.stubs;


import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.api.BluetoothUp;
import proto.robotcar.service.api.CarDataProviderServiceGrpc.CarDataProviderServiceImplBase;
import proto.robotcar.service.api.CarValueData;
import proto.robotcar.service.api.Subscription;
import proto.robotcar.service.api.Subscription.Type;
import proto.robotcar.service.core.cardataprovider.api.CarDataListener;
import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;

@Slf4j
public class CarDataProviderServiceImpl extends CarDataProviderServiceImplBase implements CarDataListener {

	@Inject
	CarDataProvider proxyListener;
	
	List<StreamObserver<CarValueData>> listener = new ArrayList<StreamObserver<CarValueData>>();
	
	public CarDataProviderServiceImpl() {
//		proxyListener.addCarDataListener(this);
	}
	
	@Override
	public void subscribeOnCarValueData(Subscription request, StreamObserver<CarValueData> responseObserver) {
		
		if(request.getType().equals(Type.REGISTER)) {
			
			if(listener.isEmpty()) {
				log.info("Add Proxy Listener");
				proxyListener.addCarDataListener(this);
			}
			
			log.info("Add a remote listener");
			listener.add(responseObserver);									
		}
		else {
			listener.remove(responseObserver);
			
			if(listener.isEmpty()) {
				proxyListener.removeCarDataListener(this);
			}			
		}

	}

	@Override
	public void subscribeOnBluetoothConnection(Subscription request, StreamObserver<BluetoothUp> responseObserver) {
		log.warn("subscribeOnBluetoothConnection: Not implemented");
	}

	@Override
	public void onNewCarDataReceived(proto.robotcar.service.caraccess.carconnector.api.CarValueData carData) {
		log.info("onNewCarDataReceived");
		CarValueData value = CarValueData.newBuilder().
			setDrivingDirection(carData.getDrivingDirection()).
			setDrivingState(carData.getDrivingState()).build();
		
		for(StreamObserver<CarValueData> l : listener) {
			l.onNext(value);
		}

	}

	@Override
	public void onBTConnectionToCarUp() {
		log.warn("onBTConnectionToCarUp: Not implemented");
	}



	
}
