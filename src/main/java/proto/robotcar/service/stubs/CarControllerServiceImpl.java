package proto.robotcar.service.stubs;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.api.CarControllerServiceGrpc.CarControllerServiceImplBase;
import proto.robotcar.service.api.EmptyRequest;
import proto.robotcar.service.api.SucceededReply;
import proto.robotcar.service.core.carcontroller.api.CarController;

@Slf4j
@Singleton
public class CarControllerServiceImpl extends CarControllerServiceImplBase{

	@Inject
	CarController carController;
	
	@Override
	public void start(EmptyRequest request, StreamObserver<SucceededReply> responseObserver) {
		
		log.info("start");
		carController.start();
		
		SucceededReply result = SucceededReply.newBuilder().setSuccess(true).build();
		responseObserver.onNext(result);
		responseObserver.onCompleted();	
	}

	@Override
	public void stop(EmptyRequest request, StreamObserver<SucceededReply> responseObserver) {

		log.info("stop");
		carController.stop();	
		
		SucceededReply result = SucceededReply.newBuilder().setSuccess(true).build();
		responseObserver.onNext(result);
		responseObserver.onCompleted();		
	}

}
