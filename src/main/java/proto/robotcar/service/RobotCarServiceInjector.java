package proto.robotcar.service;

import com.google.inject.AbstractModule;

import proto.robotcar.service.api.CarControllerServiceGrpc.CarControllerServiceImplBase;
import proto.robotcar.service.caraccess.carconnector.api.CarConnectorService;
import proto.robotcar.service.caraccess.carconnector.impl.CarConnectorServiceImpl;
import proto.robotcar.service.core.carcontroller.api.CarController;
import proto.robotcar.service.core.carcontroller.impl.CarControllerImpl;
import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;
import proto.robotcar.service.core.cardataprovider.impl.CarDataProviderImpl;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightController;
import proto.robotcar.service.core.trafficlightcontroller.impl.TrafficLightControllerServiceImpl;
import proto.robotcar.service.stubs.CarControllerServiceImpl;

public class RobotCarServiceInjector extends AbstractModule {

	@Override
	protected void configure() {		
		bind(CarController.class).to(CarControllerImpl.class);
		bind(CarControllerServiceImplBase.class).to(CarControllerServiceImpl.class);
		bind(TrafficLightController.class).to(TrafficLightControllerServiceImpl.class);
		bind(CarConnectorService.class).to(CarConnectorServiceImpl.class);
		bind(CarDataProvider.class).to(CarDataProviderImpl.class);
	}

}
