package proto.robotcar.service.mocks;

import com.google.inject.AbstractModule;

import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;

public class RobotCarServiceInjectorMock extends AbstractModule {

	@Override
	protected void configure() {
		bind(CarDataProvider.class).to(CarDataProviderMock.class);
	}

}
