package proto.robotcar.service.mocks;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.caraccess.carconnector.api.CarValueData;
import proto.robotcar.service.core.cardataprovider.api.CarDataListener;
import proto.robotcar.service.core.cardataprovider.api.CarDataProvider;

@Slf4j
@Singleton
public class CarDataProviderMock implements CarDataProvider, Runnable {

	List<CarDataListener> listeners = new ArrayList<CarDataListener>();
	
	@Override
	public void addCarDataListener(CarDataListener listener) {
		log.info("Add listener "+listener.getClass());
		listeners.add(listener);
		listener.onBTConnectionToCarUp();
	}

	@Override
	public void removeCarDataListener(CarDataListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void run() {

		log.info("Starting Car Data Provider Mock");
		
		int direction = 0;
		
		while(true)	{
			try {
				for(CarDataListener l : listeners) {
					direction++;
					CarValueData carData = new CarValueData("DRIVING", String.valueOf(direction % 360));
					l.onNewCarDataReceived(carData);
				}
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
