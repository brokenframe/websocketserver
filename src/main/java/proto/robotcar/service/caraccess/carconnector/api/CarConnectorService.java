package proto.robotcar.service.caraccess.carconnector.api;

public interface CarConnectorService extends CarMessageController {

	void connect();

	void startDriving();
	
	void stopDriving();
	
	void disconnect();

}
