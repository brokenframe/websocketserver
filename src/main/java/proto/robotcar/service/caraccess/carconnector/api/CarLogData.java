package proto.robotcar.service.caraccess.carconnector.api;

import lombok.Data;

@Data
public class CarLogData {
	
	private final String id;
	private final String level;
	private final String msg;
}
