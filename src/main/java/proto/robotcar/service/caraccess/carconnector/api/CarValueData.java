package proto.robotcar.service.caraccess.carconnector.api;

import lombok.Data;

@Data
public class CarValueData {
	private final String drivingState;
	private final String drivingDirection;
}
