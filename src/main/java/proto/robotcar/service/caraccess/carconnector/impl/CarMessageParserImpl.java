package proto.robotcar.service.caraccess.carconnector.impl;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.caraccess.carconnector.api.CarLogData;
import proto.robotcar.service.caraccess.carconnector.api.CarMessageListener;
import proto.robotcar.service.caraccess.carconnector.api.CarValueData;

@Slf4j
public class CarMessageParserImpl {

	private final Gson gson = new Gson();
	private final JsonParser parser = new JsonParser();
	private final Set<CarMessageListener> listeners = new CopyOnWriteArraySet<>();

	public CarMessageParserImpl() {
	}

	public void addCarMessageListener(CarMessageListener listener) {
		listeners.add(listener);
	}

	public void parseMessage(final String rawMessage) {
		log.debug(rawMessage);
		try {
			parseMessageUncatched(rawMessage);
		} catch (Exception e) {
			log.debug(
					"Couldn't parse car message. Error was " + e.getMessage() + ". Message was '" + rawMessage + "'.");
		}
	}

	private void parseMessageUncatched(final String rawMessage) {

		final JsonElement rawParsedMessage = parser.parse(rawMessage);
		if (!rawMessage.isEmpty()) {
			final JsonObject jsonObject = rawParsedMessage.getAsJsonObject();
			final String type = jsonObject.get("type").getAsString().toLowerCase();

			JsonElement jsonElement = jsonObject.get("data");
			if (type.equals("log")) {
				final CarLogData carLogData = gson.fromJson(jsonElement, CarLogData.class);
				if (carLogData != null)
					listeners.stream().forEach(l -> l.onNewLogMessage(carLogData.toString()));
			} else if (type.equals("cardata")) {
				final CarValueData carValueData = gson.fromJson(jsonElement, CarValueData.class);
				if (carValueData != null)
					listeners.stream().forEach(l -> l.onNewCarValueData(carValueData));
			} else if (type.equals("heartbeat")) {
				final String currentMillisString = jsonElement.getAsString();
				final long currentMillis = Long.parseLong(currentMillisString);
				if (currentMillisString != null)
					listeners.stream().forEach(l -> l.onNewHeartbeat(currentMillis));
			} else {
				log.warn("Unknown car message type " + type + ".");
			}

		}

	}

	public void removeCarMessageListener(CarMessageListener listener) {
		listeners.remove(listener);
	}
}
