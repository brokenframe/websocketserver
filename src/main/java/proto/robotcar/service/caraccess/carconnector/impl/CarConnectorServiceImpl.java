package proto.robotcar.service.caraccess.carconnector.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.caraccess.carconnector.api.CarConnectorService;
import proto.robotcar.service.caraccess.carconnector.api.CarMessageListener;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightController;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightListener;
import proto.robotcar.service.core.trafficlightcontroller.api.TrafficLightState;
import proto.robotcar.service.core.util.CLI;

@Slf4j
@Singleton
public class CarConnectorServiceImpl implements CarConnectorService, TrafficLightListener {

	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	private final CarConnectorImpl carConnector = new CarConnectorImpl(new CarMessageParserImpl());
	
	@Inject
	private TrafficLightController trafficLightController;
	
	@Override
	public void connect()
	{
		executor.submit(() -> {
			Thread.currentThread().setName("CarConnectorServiceThread");
			carConnector.connect2car();
		});
		trafficLightController.addTrafficLightListener(this);
	}
	
	@Override
	public void addCarMessageListener(CarMessageListener listener) {
		carConnector.addCarMessageListener(listener);
	}
	
	@Override
	public void removeCarMessageListener(CarMessageListener listener) {
		carConnector.removeCarMessageListener(listener);
	}
	
	@Override
	public void disconnect()
	{
		trafficLightController.removeTrafficLightListener(this);
		carConnector.disconnectFromCar();
		try {
		    log.info("Start stopping.");
		    executor.shutdown();
		    executor.awaitTermination(5, TimeUnit.SECONDS);
		}
		catch (InterruptedException e) {
			log.warn("CarConnecterService task interrupted.");
		}
		finally {
		    if (!executor.isTerminated()) {
		        log.warn("Canceling non-finished task.");
		    }
		    executor.shutdownNow();
		    log.info("End stopping.");
		}
	}

	@Override
	public void startDriving() {
		System.out.println(CLI.syncExecute("sendcmd2robotcar.sh START"));
	}

	@Override
	public void stopDriving() {
		CLI.syncExecute("sendcmd2robotcar.sh STOP");
	}

	@Override
	public void onStateChanged(TrafficLightState trafficLightState) {
		CLI.syncExecute("sendcmd2robotcar.sh LIGHT|" + trafficLightState);
	}
	
}
