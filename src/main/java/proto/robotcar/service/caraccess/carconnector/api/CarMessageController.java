package proto.robotcar.service.caraccess.carconnector.api;

public interface CarMessageController {
	
	void addCarMessageListener(CarMessageListener listener); 
	
	void removeCarMessageListener(CarMessageListener listener);
	
}
