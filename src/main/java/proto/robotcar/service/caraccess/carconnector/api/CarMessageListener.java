package proto.robotcar.service.caraccess.carconnector.api;

public interface CarMessageListener {

	void onNewLogMessage(String message);
	
	void onNewCarValueData(CarValueData carData);

	void onNewHeartbeat(long currentMillis);
}
