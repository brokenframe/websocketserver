package proto.robotcar.service.caraccess.carconnector.api;

import proto.robotcar.service.core.errorsimulator.api.ErrorCase;

public interface CarConnector extends CarMessageController {
	
	void connect2car();
	
	void disconnectFromCar();
	
	void startCar();
	
	void stopCar();
	
	void errorCaseActivated(ErrorCase errorCase);
	
	void errorCaseDeactivated(ErrorCase errorCase);
	
}
