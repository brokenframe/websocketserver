package proto.robotcar.service.caraccess.carconnector.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import lombok.extern.slf4j.Slf4j;
import proto.robotcar.service.caraccess.carconnector.api.CarMessageListener;
import proto.robotcar.service.util.JsonHelper;

@Slf4j
public class CarConnectorImpl {

	private CarMessageParserImpl carMessageParserImpl;
	private BufferedReader reader;
	private CountDownLatch waitForConnection = new CountDownLatch(1);

	public CarConnectorImpl(CarMessageParserImpl carMessageParserImpl) {
		this.carMessageParserImpl = carMessageParserImpl;
	}

	public void addCarMessageListener(CarMessageListener listener) {
		carMessageParserImpl.addCarMessageListener(listener);
	}

	public void connect2car() {
		try {
			openSerialConnectionReader();
		} catch (FileNotFoundException e) {
			log.error("Couldn't read from BT device. Either connection not established before disconnecting or BT not configured correctly.");
			waitForConnection.countDown();
		}
		try {
			readSerialConnectionByLine();
		} catch (IOException e) {
			log.info("Reading from /dev/rfcomm0 stopped.");
		}
	}

	private void readSerialConnectionByLine() throws IOException {
		String line = "";
		while ((line = reader.readLine()) != null) {
			if (!line.isEmpty()) {
				if (JsonHelper.isValidJson(line)) {
					carMessageParserImpl.parseMessage(line);
				} else {
					log.debug("No standard log message received. Ignoring. Content was '" + line + "'.");
				}
			}
		}
	}

	private void openSerialConnectionReader() throws FileNotFoundException {
		reader = new BufferedReader(new FileReader("/dev/rfcomm0"));
		waitForConnection.countDown();
	}

	public void disconnectFromCar() {
		waitForConnection();
		closeSerialReader();
	}

	private void waitForConnection() {
		try {
			waitForConnection.await();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		waitForConnection = new CountDownLatch(1);
	}

	private void closeSerialReader() {
		if (reader != null) {
			closeReaderSilently();
			reader = null;
		}
	}

	private void closeReaderSilently() {
		try {
			reader.close();
		} catch (IOException e) {
		}
	}

	public void removeCarMessageListener(CarMessageListener listener) {
		carMessageParserImpl.removeCarMessageListener(listener);
	}

}
