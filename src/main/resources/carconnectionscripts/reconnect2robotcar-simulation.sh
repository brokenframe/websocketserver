#!/bin/bash
uptime=0
while [ true ]
do
    echo "{\"type\":\"HEARTBEAT\",\"data\":\"$uptime\"}"
    sleep 5
    uptime=$((uptime+5))
done
