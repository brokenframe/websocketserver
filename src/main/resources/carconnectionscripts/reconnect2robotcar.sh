#!/bin/bash
lockfile=/var/lock/reconnect2robotcar
{
  if ! flock -n 9
  then
    echo "Script already running. Please stop running script first." 2>&1
    exit 1
  fi

  if [ -e /dev/rfcomm0 ]; then
    echo "Bluetooth port already there."
    exit 0
  else
    echo "Bluetooth port will be binded to /dev/rfcomm0"
    rfcomm bind rfcomm0 98:D3:32:20:71:C5
  fi

  while [ ! -e /dev/rfcomm0 ]; do
    echo "Waiting 1 sec for bluetooth connection..."
    sleep 1
  done

} 9>"$lockfile"